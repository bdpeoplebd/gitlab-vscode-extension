const vscode = require('vscode');
const gitLabService = require('../gitlab_service');
const { SidebarTreeItem } = require('../sidebar_tree_item');

const { alwaysShowAllIssueInfo } = vscode.workspace.getConfiguration('gitlab');

const sortTypes = new Map([
  ['IID', 0],
  ['IID_REVERSE', 0],
  ['TITLE', 3],
  ['TITLE_REVERSE', 3],
  ['UPVOTES', 1],
  ['UPVOTES_REVERSE', 1],
  ['DOWNVOTES', 1],
  ['DOWNVOTES_REVERSE', 1],
  ['TASK_COUNT', 2],
  ['TASK_COUNT_REVERSE', 2],
]);

class IssueData {
  constructor(issue, issuableSign) {
    this.issue = issue;
    this.issuableSign = issuableSign;
  }

  getId() {
    return `${this.issuableSign}${this.issue.iid}`;
  }

  getTitle() {
    return `${this.issue.title}`;
  }

  getVotes(autofill = false) {
    if (!this.issue.upvotes && !this.issue.downvotes) {
      if (autofill) {
        return '↑0↓0';
      }
      return null;
    }
    return `↑${this.issue.upvotes}↓${this.issue.downvotes}`;
  }

  getTasks(autofill = false) {
    if (!this.issue.has_tasks) {
      if (autofill) {
        return 'n୵a';
      }
      return null;
    }
    return `${this.issue.task_completion_status.completed_count}୵${
      this.issue.task_completion_status.count
    }`;
  }
}

class DataProvider {
  constructor({ fetcher, issuableType, noItemText }) {
    this._onDidChangeTreeData = new vscode.EventEmitter();
    this.onDidChangeTreeData = this._onDidChangeTreeData.event;

    this.fetcher = fetcher;
    this.issuableSign = issuableType === 'merge request' ? '!' : '#';
    this.noItemText = noItemText || 'Nothing to show.';
    this.projects = [];
    this.refreshUpdate = true;
    this.sortIndex = 0;
    this.sortUpdate = false;
  }

  async getProjectIssues(project) {
    if (this.sortUpdate) {
      this.sortUpdate = false;
      if (++this.sortIndex >= sortTypes.size) {
        this.sortIndex = 0;
      }
    }
    const items = [];
    const issues = await project.issues;
    if (issues.length) {
      const sortName = Array.from(sortTypes.keys())[this.sortIndex];
      const sortIndex = sortTypes.get(sortName);
      const reversed = sortName.endsWith('_REVERSE');
      const pos = !reversed ? 1 : -1;
      const neg = !reversed ? -1 : 1;
      if (sortName === 'IID' || sortName === 'IID_REVERSE') {
        issues.sort((a, b) => (a.iid > b.iid ? pos : neg));
      } else if (sortName === 'UPVOTES' || sortName === 'UPVOTES_REVERSE') {
        issues.sort((a, b) => (a.upvotes > b.upvotes ? pos : neg));
      } else if (sortName === 'DOWNVOTES' || sortName === 'DOWNVOTES_REVERSE') {
        issues.sort((a, b) => (a.downvotes > b.downvotes ? pos : neg));
      } else if (sortName === 'TASK_COUNT' || sortName === 'TASK_COUNT_REVERSE') {
        issues.sort((a, b) =>
          a.task_completion_status.count - a.task_completion_status.completed_count >
          b.task_completion_status.count - b.task_completion_status.completed_count
            ? pos
            : neg,
        );
      } else if (sortName === 'TITLE' || sortName === 'TITLE_REVERSE') {
        issues.sort((a, b) => (a.title > b.title ? pos : neg));
      }
      issues.forEach(issue => {
        const issueData = new IssueData(issue, this.issuableSign);
        const titleItems = [
          issueData.getId(),
          issueData.getVotes(
            alwaysShowAllIssueInfo ||
              sortName.startsWith('UPVOTES') ||
              sortName.startsWith('DOWNVOTES'),
          ),
          issueData.getTasks(alwaysShowAllIssueInfo || sortName.startsWith('TASK')),
          issueData.getTitle(),
        ];
        const element = titleItems[sortIndex];
        titleItems.splice(sortIndex, 1);
        titleItems.splice(0, 0, element);
        let title = '';
        for (let i = 0; i < titleItems.length; i++) {
          if (titleItems[i] == null) {
            continue;
          }
          if (i > 0 && (i !== 1 || titleItems[0] != null)) {
            title += ' · ';
          }
          title += titleItems[i];
        }
        items.push(new SidebarTreeItem(title, issue, null, project.uri));
      });
    } else {
      items.push(new SidebarTreeItem(this.noItemText));
    }
    return items;
  }

  async getChildren(el) {
    let items = [];
    if (this.refreshUpdate) {
      this.refreshUpdate = false;
      this.projects = await gitLabService[this.fetcher]();
    }

    if (el) {
      if (el.contextValue && el.contextValue.startsWith('uri-')) {
        const uri = el.contextValue.split('uri-')[1];
        const project = this.projects.filter(p => p.uri === uri);
        if (project.length === 1) {
          items = await this.getProjectIssues(project[0]);
        }
      }
    } else if (this.projects.length > 1) {
      this.projects.forEach(project => {
        items.push(
          new SidebarTreeItem(
            project.project,
            { uri: project.uri },
            vscode.TreeItemCollapsibleState.Collapsed,
          ),
        );
      });
    } else if (this.projects.length === 1) {
      items = await this.getProjectIssues(this.projects[0]);
    } else {
      items.push(new SidebarTreeItem(this.noItemText));
    }

    return items;
  }

  getParent() {
    return null;
  }

  getTreeItem(item) {
    return item;
  }

  refresh() {
    this.refreshUpdate = true;
    this._onDidChangeTreeData.fire();
  }

  sort() {
    this.sortUpdate = true;
    this._onDidChangeTreeData.fire();
  }
}

exports.DataProvider = DataProvider;
