const vscode = require('vscode');

class SidebarTreeItem extends vscode.TreeItem {
  constructor(title, data, collapsibleState, uri) {
    super(title, collapsibleState);

    const { enableExperimentalFeatures } = vscode.workspace.getConfiguration('gitlab');

    if (data) {
      let command = 'gl.showRichContent';
      let arg = [data, uri];

      if (data.web_url && (data.sha || !enableExperimentalFeatures)) {
        command = 'vscode.open';
        arg = [vscode.Uri.parse(data.web_url)];
      }

      if (typeof data.uri !== 'undefined') {
        this.contextValue = `uri-${data.uri}`;
      } else {
        this.command = {
          command,
          arguments: arg,
        };
      }
    }
  }
}

exports.SidebarTreeItem = SidebarTreeItem;
